# The Jobs Service

Any application will need a Queue Service to execute jobs asynchronously. Its
purpose is to receive multi-step job descriptions in JSON (essentially
consisting of a series of requests to make, e.g. "make a PUT to this URL with
this body, then a POST to this URL with this body, then a DELETE to this URL".
The steps are named.

The Queue Service accepts the job, enqueues it in a FIFO queue, and immediately
returns a QueueJob resource to the caller.

The AsyncJob resource can be polled via GET to check the status of the job. This
is a very efficient operation since the result is cached aggressively in
Varnish. Thus, a web client can poll an AsyncJob several times a second if
necessary, e.g. to update a progress bar or to check for errors.

An AsyncJob can also be DELETEd from the queue.

Use cases: to start an update operation that might take time: book a ticket,
send an email, upload and thumbnail a photograph, perform a search, etc.


## Job Workers

The same code is used for the Job service and its workers. They are deployed
in separate containers, `job_service` and `job_workers`.


## Sample AsyncJob

The following will create an AsyncJob which will turn to poison after about 35 seconds.
Raising the `retry_exponent` to 2.5 yields poison in about a minute. Raising it to 3
extends this to almost 2 minutes.
```
  AsyncJob.create(
    steps: [{'url' => 'http://127.0.0.1', 'retry_exponent' => 2}],
    credentials: "bWFnbmV0bzp4YXZpZXI=",
    token: "hhhhhhhhhhhhhhh")
```


## Installation

First start the common infrastructure, if it isn't already running:
```
  docker-compose -f ../ocean/common.yml up --build
```
Then, to run only the Jobs service:
```
  docker-compose -f ../ocean/ocean.yml up --build jobs_service
```
Then run the service setup/update rake task:
```
  docker-compose -f ../ocean/ocean.yml run --rm jobs_service rake ocean:setup_all
```

The `ocean:setup_all` task executes the following rake task:
```
  ocean:setup_dynamodb
  ocean:install_cron_jobs
```
It can be run at any time. You will want to run `ocean:setup_all` after cron
jobs have changed.


## Running the RSpecs

If you intend to run RSpec tests, start the common infrastructure, if it isn't
already running:
```
  docker-compose -f ../ocean/common.yml up --build
```
Then, to run the actual specs:
```
  docker-compose -f ../ocean/ocean.yml run --rm -e LOG_LEVEL=fatal jobs_service rspec
```
