$dynamo_client = Aws::DynamoDB::Client.new(aws_config(endpoint: 'AWS_DYNAMODB_ENDPOINT'))

sqs_client = Aws::SQS::Client.new(aws_config(endpoint: 'AWS_SQS_ENDPOINT'))
$sqs = Aws::SQS::Resource.new(client: sqs_client)
