Jobs::Application.routes.draw do

  get "/alive" => "alive#index"
  put "/execute_cron_jobs" => "cron_jobs#execute"

  scope "v1" do
    resources :async_jobs, only: [:create, :show, :destroy], 
                           constraints: {id: UUID_REGEX} do
      collection do
      	put "cleanup"
      end
    end

    resources :cron_jobs, only: [:index, :create, :show, :update, :destroy], 
                           constraints: {id: UUID_REGEX} do
      member do
        put 'run'
      end
    end

    resources :scheduled_jobs, only: [:index, :create, :show, :update, :destroy], 
                           constraints: {id: UUID_REGEX} do
      collection do
        put "execute"
      end
      member do
        put 'run'
      end
    end
  end

end
