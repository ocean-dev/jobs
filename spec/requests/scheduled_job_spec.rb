require 'spec_helper'

describe ScheduledJob, :type => :request do

  before :each do
    permit_with 200
  end


  describe "run" do

    it "should create an AsyncJob" do
      expect(AsyncJob).to receive(:create!)
      job = create :scheduled_job, id: 'a-b-c-d-e'
      permit_with 200
      put "/v1/scheduled_jobs/a-b-c-d-e/run", 
          {}, 
          {'HTTP_ACCEPT' => "application/json", 'X-API-TOKEN' => "incredibly-fake"}
      expect(response.status).to eq 204
      expect(response.body).to be_blank
    end

  end
end 
