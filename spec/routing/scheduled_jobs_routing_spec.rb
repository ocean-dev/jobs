require "spec_helper"

describe ScheduledJobsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(get("/v1/scheduled_jobs")).to route_to("scheduled_jobs#index")
    end

    it "routes to #create" do
      expect(post("/v1/scheduled_jobs")).to route_to("scheduled_jobs#create")
    end

    it "routes to #show" do
      expect(get("/v1/scheduled_jobs/a-b-c-d-e")).to route_to("scheduled_jobs#show", :id => "a-b-c-d-e")
    end

    it "routes to #update" do
      expect(put("/v1/scheduled_jobs/a-b-c-d-e")).to route_to("scheduled_jobs#update", :id => "a-b-c-d-e")
    end

    it "routes to #destroy" do
      expect(delete("/v1/scheduled_jobs/a-b-c-d-e")).to route_to("scheduled_jobs#destroy", :id => "a-b-c-d-e")
    end

    it "routes to #run" do
      expect(put("/v1/scheduled_jobs/a-b-c-d-e/run")).to route_to("scheduled_jobs#run", :id => "a-b-c-d-e")
    end

    it "routes to #execute" do
      expect(put("/v1/scheduled_jobs/execute")).to route_to("scheduled_jobs#execute")
    end
  end
end
