require 'spec_helper'

describe ScheduledJobsController, :type => :controller do  

  describe "PUT execute" do

    before :each do
      ScheduledJob.establish_db_connection
      ScheduledJob.delete_all
      permit_with 200
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "incredibly-fake!"
    end


    it "should return JSON" do
      put :execute
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      put :execute
      expect(response.status).to eq 400
    end
    
    it "should return a 204 when successful" do
      put :execute
      expect(response.status).to eq 204
    end

    it "should not return a body" do
      expect(ScheduledJob).to receive :process_queue
      put :execute, body: nil
      expect(response.body).to be_blank
    end

  end

end
