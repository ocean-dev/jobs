require 'spec_helper'

describe ScheduledJob, :type => :model do

  before :each do
    ScheduledJob.establish_db_connection
    AsyncJob.establish_db_connection
    ScheduledJob.delete_all
    AsyncJob.delete_all
    allow(Object).to receive(:sleep)
  end

  after :each do
    ScheduledJob.delete_all
    AsyncJob.delete_all
  end


  describe "attributes" do
    
    it "should have a UUID" do
      expect(create(:scheduled_job).id).to be_a String
    end

    it "should assign a UUID to the hash_key attribute if nil at create" do
      i = build :scheduled_job, id: nil
      expect(i.save).to eq true
      expect(i.id).not_to be_blank
    end

    it "should have a creation time" do
      expect(create(:scheduled_job).created_at).to be_a Time
    end

    it "should have an update time" do
      expect(create(:scheduled_job).updated_at).to be_a Time
    end
  
    it "should have a creator" do
      expect(create(:scheduled_job).created_by).to be_a String
    end

    it "should have an updater" do
      expect(create(:scheduled_job).updated_by).to be_a String
    end


    it "should have a required credentials attribute" do
      expect(create(:scheduled_job).credentials).to be_a String
      expect(build(:scheduled_job, credentials: nil)).not_to be_valid
    end

    it "should require the credentials to be unscramblable (is that a word?)" do
      expect(build(:scheduled_job, credentials: 'bWFnbmV0bzp4YXZpZXI=')).to be_valid
      expect(build(:scheduled_job, credentials: 'blahonga')).not_to be_valid
    end

    it "should only require the credentials at creation time" do
      j = create :scheduled_job, credentials: 'bWFnbmV0bzp4YXZpZXI='
      expect(j).to be_valid
      j.credentials = ""
      expect(j).to be_valid
      j.save!
    end

    it "should not have a token attribute" do
      expect(create(:scheduled_job)).to_not respond_to :token
    end


    it "should have a steps array" do
     expect(create(:scheduled_job, steps: [{}, {}, {}]).steps).to eq [{}, {}, {}]
    end


    it "should have a run_at datetime" do
      expect(create(:scheduled_job).run_at).to be_a Time
      expect(build(:scheduled_job, run_at: nil)).not_to be_valid
    end


    it "should have a lock_version attribute" do
      expect(build(:scheduled_job)).to respond_to(:lock_version)
    end

    it "should have an enabled attribute defaulting to true" do
      expect(create(:scheduled_job).enabled).to be true
    end

    it "should have name attribute" do
      expect(create(:scheduled_job).name).to eq ""
    end

    it "should have a description attribute" do
      expect(create(:scheduled_job).description).to eq ""
    end

    it "should have a max_seconds_in_queue attribute" do
      expect(create(:scheduled_job).max_seconds_in_queue).to eq 1.day
    end

    it "should have a default_poison_limit attribute" do
      expect(create(:scheduled_job).default_poison_limit).to eq 5
    end

    it "should have a default_step_time attribute" do
      expect(create(:scheduled_job).default_step_time).to eq 30
    end

    it "should have a last_run_at time" do
      expect(build(:scheduled_job)).to respond_to(:last_run_at)
    end

    it "should have a last_async_job_id attribute" do
      expect(build(:scheduled_job)).to respond_to(:last_async_job_id)
    end

    it "should have a poison_email attribute" do
      expect(create(:scheduled_job)).to respond_to :poison_email
    end

    it "should allow blank poison_email addresses" do
      expect(build(:scheduled_job, poison_email: "")).to be_valid
    end

    it "should require a valid poison_email address" do
      expect(build(:scheduled_job, poison_email: "john@@doe")).not_to be_valid
    end

    it "should not consider poison_email addresses with names valid" do
      expect(build(:scheduled_job, poison_email: "John Doe <john@doe.com>")).not_to be_valid
    end


    it "should let run_at (and others) be mass assignable" do
      t1 = 1.year.from_now.utc
      j = create :scheduled_job, run_at: t1
      j.reload
      expect(j.run_at.to_i).to eq t1.to_i
      t2 = 10.years.ago.utc
      j.update_attributes(run_at: t2)
      j.reload
      expect(j.run_at.to_i).to eq t2.to_i
    end
  end


  describe "due?" do

    it "should exist as a predicate" do
      expect(build :scheduled_job).to respond_to :due?
    end

    it "should take a Time as its argument" do
      expect(create(:scheduled_job, run_at: 30.seconds.ago.utc).due?(Time.now.utc)).to be true
    end
  end



  describe "process_job" do 

    it "should do nothing unless the job is enabled" do
      job = create :scheduled_job, enabled: false
      expect(ScheduledJob.all.length).to eq 1
      expect(job).to_not receive(:due?)
      expect(job).to_not receive(:post_async_job)
      job.process_job
    end

    it "should do nothing unless the time is due" do
      job = create :scheduled_job
      expect(ScheduledJob.all.length).to eq 1
      expect(job).to receive(:due?).and_return(false)
      expect(job).to_not receive(:post_async_job)
      job.process_job
    end

    describe "when it runs a scheduled job" do

      it "should call post_async_job" do
        job = create :scheduled_job
        expect(ScheduledJob.all.length).to eq 1
        expect(job).to receive(:due?).and_return(true)
        expect(job).to receive(:post_async_job).and_return "the-async-job-uuid"
        job.process_job
      end

      it "should set the last_run_at attribute" do
        job = create :scheduled_job
        expect(ScheduledJob.all.length).to eq 1
        expect(job).to receive(:due?).and_return(true)
        expect(job).to receive(:post_async_job).and_return "the-async-job-uuid"
        expect(job).to receive(:last_run_at=).with(an_instance_of Time)
        expect(job).to receive(:save!)
        job.process_job
      end

      it "should set the last_async_job_id attribute" do
        job = create :scheduled_job
        expect(ScheduledJob.all.length).to eq 1
        expect(job).to receive(:due?).and_return(true)
        expect(job).to receive(:post_async_job).and_return "the-async-job-uuid"
        expect(job).to receive(:last_async_job_id=).with("the-async-job-uuid")
        expect(job).to receive(:save!)
        job.process_job
      end

      it "should disable it after running it" do
        job = create :scheduled_job
        expect(ScheduledJob.all.length).to eq 1
        expect(job).to receive(:due?).and_return(true)
        expect(job).to receive(:post_async_job).and_return "the-async-job-uuid"
        expect(job).to receive(:enabled=).with(false)
        expect(job).to receive(:save!)
        job.process_job
      end
    end
  end


  describe "post_async_job" do
    
    it "should create an AsyncJob from the ScheduledJob" do                
      expect_any_instance_of(AsyncJob).to receive(:enqueue)
      job = create :scheduled_job, steps: [{}, {}]
      job.post_async_job
    end

    it "should return the AsyncJob uuid" do
      expect_any_instance_of(AsyncJob).to receive(:enqueue)
      job = create :scheduled_job, steps: [{}, {}]
      expect(job.post_async_job).to be_a String
    end
  end

end
