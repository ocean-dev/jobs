# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :scheduled_job do
    credentials { "bWFnbmV0bzp4YXZpZXI=" }
    run_at      { 1.day.from_now.utc }
    created_by  { "https://api.example.com/v1/api_users/1-2-3-4-5" }
    updated_by  { "https://api.example.com/v1/api_users/a-b-c-d-e" }
  end
end
