namespace :ocean do

  desc 'Sets up the Ocean cron jobs.'
  task install_cron_jobs: :environment do

    jobs = []
    @okay = Dir.glob("#{Rails.root}/config/cron_jobs/*.json").map do |f|
      j = JSON.parse(File.read(f))
      only_in = j['only_in_env']
      next if only_in && (only_in.is_a?(Array) ? !only_in.include?(OCEAN_ENV) : only_in != OCEAN_ENV)
      credentials = ::Base64.strict_encode64 "#{j['api_user']}:#{j['password']}"
      jobs << {
        "name" => j['id'],
        "description" => j['description'],
        "cron" => j['cron'],
        "steps" => j['steps'],
        "credentials" => credentials,
        "enabled" => (j.has_key?('enabled') ? j['enabled'] : true),
        "default_step_time" => j['default_step_time'] || 30,
        "max_seconds_in_queue" => j['max_seconds_in_queue'] || 1.day,
        "default_poison_limit" => j['default_poison_limit'] || 5
      }
    end

    CronJob.maintain_all jobs

  end
end
