#
# This task creates the config/cron_jobs.yml file out of the contents
# of the config/global folder.
#

namespace :ocean do

  desc 'Sets up the Ocean cron jobs.'
  task setup_all: :environment do
    Rake::Task["ocean:setup_dynamodb"].invoke
    Rake::Task["ocean:install_cron_jobs"].invoke
  end
end
