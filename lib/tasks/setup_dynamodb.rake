#
# This task sets up DynamoDB tables.
#

namespace :ocean do
  desc 'Connects to the DynamoDB tables, effectively creating them if not already created'
  task setup_dynamodb: :environment do
    require 'async_job'
    require 'cron_job'
    require 'scheduled_job'

    puts "============================================================"
    puts "Setting up AsyncJob..."
    AsyncJob.establish_db_connection
    puts "Done."

    puts "Setting up CronJob..."
    CronJob.establish_db_connection
    puts "Done."

    puts "Setting up ScheduledJob..."
    ScheduledJob.establish_db_connection
    puts "Done."

    puts "All done."

  end
end
