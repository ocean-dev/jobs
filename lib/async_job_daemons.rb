require 'dotenv'
Dotenv.load('/srv/.env')

# The directory RUN_TIME_DIR must already exist, and this process
# must have the right to create files in it.
RUN_TIME_DIR = ENV['RUN_TIME_DIR'] || '/var/run'

# The number of workers to keep running at all times
N_WORKERS = ENV['N_WORKERS'].to_i

# Individual worker log files and/or log to syslog?
WORKER_LOG_OUTPUT = ENV['WORKER_LOG_OUTPUT'] || false
LOG_OUTPUT_SYSLOG = ENV['LOG_OUTPUT_SYSLOG'] || false

ENV['RAILS_ENV'] ||= 'production'

ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)
require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])

ENV_PATH = File.expand_path('../../config/environment.rb', __FILE__)

require 'daemons'

options = {
  dir_mode:          :system,
  dir:               RUN_TIME_DIR,
  backtrace:         true,
  log_output:        WORKER_LOG_OUTPUT,
  log_output_syslog: LOG_OUTPUT_SYSLOG,
  multiple:          true,
  monitor:           true,
  monitor_interval:  10
}


# The async job workers
(0...N_WORKERS).each do |i|
  Daemons.run_proc("async_job_worker_#{i}", options) do
    require ENV_PATH
    AsyncJob.establish_db_connection
    q = AsyncJobQueue.new basename: ASYNCJOBQ_AWS_BASENAME
    loop do
      puts "Polling..."
      q.poll { |qm| qm.process } # rescue nil
    end
  end
end

Process.waitall
