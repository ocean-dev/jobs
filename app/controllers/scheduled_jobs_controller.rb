class ScheduledJobsController < ApplicationController

  ocean_resource_controller required_attributes: [:lock_version],
                            extra_actions: { 'execute' => ['execute', "PUT"],
                                             'run'     => ['run',     "PUT"]}
  
  before_action :find_scheduled_job, :only => [:show, :update, :destroy, :run]
    

  # GET /scheduled_jobs
  def index
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    # if stale?(collection_etag(ScheduledJob))   # collection_etag is still ActiveRecord only!
    # Instead, we get all the ScheduledJobs (they are few) and compute the ETag manually:
    @scheduled_jobs = ScheduledJob.all    
    latest = @scheduled_jobs.max_by(&:updated_at)
    last_updated = latest && latest.updated_at 
    if stale?(etag: "ScheduledJob:#{@scheduled_jobs.length}:#{last_updated}")
      api_render @scheduled_jobs
    end
  end


  # POST /scheduled_jobs
  def create
    ActionController::Parameters.permit_all_parameters = true
    @scheduled_job = ScheduledJob.new(params)
    set_updater(@scheduled_job)
    @scheduled_job.save!
    api_render @scheduled_job, new: true
  end


  # GET /scheduled_jobs/a-b-c-d-e
  def show
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    if stale?(etag: @scheduled_job.lock_version,          # NB: DynamoDB tables dont have cache_key - FIX!
              last_modified: @scheduled_job.updated_at)
      api_render @scheduled_job
    end
  end


  # PUT /scheduled_jobs/a-b-c-d-e
  def update
    if missing_attributes?
      render_api_error 422, "Missing resource attributes"
      return
    end
    @scheduled_job.steps = params[:steps] if params[:steps]
    @scheduled_job.name = params[:name] if params[:name]
    @scheduled_job.description = params[:description] if params[:description]
    @scheduled_job.credentials = params[:credentials] if params[:credentials]
    @scheduled_job.max_seconds_in_queue = params[:max_seconds_in_queue] if params[:max_seconds_in_queue]
    @scheduled_job.default_poison_limit = params[:default_poison_limit] if params[:default_poison_limit]
    @scheduled_job.default_step_time = params[:default_step_time] if params[:default_step_time]
    @scheduled_job.run_at = params[:run_at] if params[:run_at]
    @scheduled_job.enabled = params[:enabled] if params[:enabled]
    @scheduled_job.lock_version = params[:lock_version] if params[:lock_version]
    @scheduled_job.poison_email = params[:poison_email] if params[:poison_email]
    set_updater(@scheduled_job)
    @scheduled_job.save!
    api_render @scheduled_job
  end


  # DELETE /scheduled_jobs/a-b-c-d-e
  def destroy
    @scheduled_job.destroy
    render_head_204
  end


  # PUT /scheduled_jobs/a-b-c-d-e/run
  def run
    @scheduled_job.last_async_job_id = @scheduled_job.post_async_job
    @scheduled_job.save!
    render_head_204
  end


  # PUT /scheduled_jobs/execute
  def execute
    ScheduledJob.process_queue
    render_head_204
  end
  
  
  private
     
  def find_scheduled_job
    ActionController::Parameters.permit_all_parameters = true
    the_id = params['uuid'] || params['id']  # 'id' when received from the Rails router, uuid othw
    @scheduled_job = ScheduledJob.find_by_key(the_id, consistent: true)
    return true if @scheduled_job
    render_api_error 404, "ScheduledJob not found"
    false
  end
    
end

