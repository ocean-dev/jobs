class AsyncJobQueue

  cattr_reader :sqs

  attr_reader :basename, :fullname
  attr_reader :queue


  def initialize(basename: "AsyncJobQueue-" + SecureRandom::urlsafe_base64)
    @@sqs ||= $sqs
    @basename = basename
    @fullname = basename + Api.basename_suffix
    @queue = AsyncJobQueue.create_queue(self)
  end


  def self.create_queue(i)
    i.sqs.create_queue(
      queue_name: i.fullname,
      attributes: { "ReceiveMessageWaitTimeSeconds" => "20" }
    )
  end


  #
  # This deletes the AWs Queue permanently, including any messages it may contain.
  #
  def delete
    queue.delete
  end


  #
  # This enqueues a message to the AWS queue.
  #
  def send_message(msg, **keywords)
    queue.send_message(message_body: msg, **keywords)
  end


  #
  # This receives a message from the AWS queue or false if there is none.
  # If called with a block, the message will be deleted after normal completion
  # of the block and nil returned. If called without a block, any message will
  # have to be deleted by the caller.
  #
  def receive_message(opts={}, &block)
    poll opts.merge({wait_time_seconds: 20}), &block
  end

  #
  # This long-polls the AWS queue. When called with a block, the message will
  # automatically be deleted when the block is exited normally. Always receives
  # exactly 1 message at a time, with receive_count.
  #
  def poll(opts={}, &block)
    opts = {
      attribute_names: ['ApproximateReceiveCount']
    }.merge(opts).merge(
      max_number_of_messages: 1
    )
    msg = queue.receive_messages(opts).first
    return false unless msg
    qmsg = QueueMessage.new(msg)
    if block
      yield qmsg
      qmsg.delete
      nil
    else
      qmsg
    end
  end

end
