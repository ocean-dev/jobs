class ScheduledJob < OceanDynamo::Table

  ocean_resource_model index: [:id], search: false


  dynamo_schema(:id, table_name_suffix: Api.basename_suffix,
                create: true, client: $dynamo_client) do
    # Input attributes
    attribute :name
    attribute :description
    attribute :credentials
    attribute :steps,                :serialized, default: []
    attribute :max_seconds_in_queue, :integer,    default: 1.day
    attribute :default_poison_limit, :integer,    default: 5
    attribute :default_step_time,    :integer,    default: 30
    attribute :run_at,               :datetime
    attribute :enabled,              :boolean,    default: true
    attribute :poison_email

    # Output only
    attribute :created_by
    attribute :updated_by
    attribute :last_run_at,          :datetime
    attribute :last_async_job_id
  end


  # Validations
  validates :credentials, presence: { message: "must be specified", on: :create }
  validates :run_at, presence: true

  validates_each :credentials, on: :create, allow_blank: true do |job, attr, val|
    username, password = Api.decode_credentials val
    job.errors.add(attr, "are malformed") if username.blank? || password.blank?
  end

  validates_each :steps do |job, attr, value|
    job.errors.add(attr, 'must be an Array') unless value.is_a?(Array)
  end

  validates :poison_email, email: { message: "is an invalid email address" }, allow_blank: true


  def due?(t = Time.now.utc)
    run_at <= t
  end


  def self.process_queue
    # No locking required since this will be called as part of the execution of a CronJob
    all.each(&:process_job)
  end


  def process_job
    return unless enabled
    return unless due?
    self.last_async_job_id = post_async_job
    Rails.logger.info "Running ScheduledJob \"#{name}\" [Job #{last_async_job_id}]."
    self.last_run_at = Time.now.utc
    self.enabled = false
    save!
  end


  def post_async_job
    begin
      aj = AsyncJob.create! credentials: credentials,
             steps: steps, max_seconds_in_queue: max_seconds_in_queue,
             default_poison_limit: default_poison_limit, default_step_time: default_step_time,
             poison_email: poison_email
      aj.uuid
    rescue StandardError => e
      Rails.logger.info "Running ScheduledJob \"#{name}\" failed to create an AsyncJob: #{e.message}."
      nil
    end
  end

end
