FROM ruby:2.5.3-alpine
LABEL net.oceanframework.project="ocean" \
      net.oceanframework.service="jobs" \
      maintainer="peter@peterbengtson.com" \
      documentation="http://wiki.oceanframework.net"

# Set locale
ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_CTYPE=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8

# Update and upgrade
RUN apk update && \
    apk upgrade && \
    apk add --no-cache --virtual .build-deps \
            build-base && \
    apk add --no-cache \
            mysql-dev && \
    apk add --no-cache \
            socat \
            tzdata \
            curl \
            findutils && \
            ln -fs /usr/share/zoneinfo/GMT /etc/localtime && \
            gem install bundler

# Create the /srv directory to run the Rails app from
RUN mkdir -p /srv
WORKDIR /srv

# Install gems
COPY Gemfile Gemfile.lock ./
RUN bundle install --jobs 25 --retry 5

# Delete build dependencies
RUN apk del .build-deps

# Install the app, except for files excluded in .dockerignore
COPY . ./

# Create the tmp directory in the app
RUN mkdir -p tmp

# Create the worker data directory
RUN mkdir -p /var/run/async_workers
RUN chmod 0777 /var/run/async_workers

# Expose the service port
EXPOSE 80

# Health check
HEALTHCHECK --interval=10s --timeout=9s \
  CMD curl -f http://localhost:80/alive || exit 1

# Set the shell command line prefix
ENTRYPOINT ["bundle", "exec"]

# Start the web server
CMD puma -C config/puma.rb
